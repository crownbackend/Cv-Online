import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { RouterModule, Routes} from '@angular/router';
import { CvComponent } from './cv/cv.component';
import { RealisationComponent } from './realisation/realisation.component';
import { ErrorComponent } from './error/error.component';
import { CompetencesComponent } from './competences/competences.component';

const appRoutes: Routes = [
  { path: '', component: MainComponent},
  { path: 'cv', component: CvComponent},
  { path: 'realisations', component: RealisationComponent},
  { path: 'competences', component: CompetencesComponent},
  { path: 'not-found', component: ErrorComponent},
  { path: '**', redirectTo: '/not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    CvComponent,
    RealisationComponent,
    ErrorComponent,
    CompetencesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
